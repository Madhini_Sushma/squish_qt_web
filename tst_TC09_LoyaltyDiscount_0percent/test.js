source(findFile("scripts", "Libraries/Application.js"))
source(findFile("scripts", "Libraries/Global.js"))
source(findFile("scripts", "Libraries/Constants.js"))
source(findFile("scripts", "Libraries/Perf.js"))


function main() 
{
    var objApp = new Application();
    var Global = new GlobalVariables();
    
    testSettings.logScreenshotOnPass = true;
    testSettings.logScreenshotOnError = true;
    testSettings.logScreenshotOnFail = true;
    testSettings.setWrappersForApplication("__squish__webhook", ("Web"))
    ctx_web = startApplication("__squish__webhook", "localhost", 8081)
    
    startBrowser("https://www.nordicnaturals.com/consumers.php");
    activateBrowserTab(waitForObject(":Welcome to Nordic Naturals_BrowserTab"));
    loadUrl("http://nordicnaturals.com/en/General_Public/Nordic_Naturals_Products/97");
    clickLink(waitForObject(":Nordic Naturals Products.Complete Omega-D3� Junior_A"));
    snooze(4);
    
    objApp.start(Global.xTupleName);
    test.pass("Start " + Global.xTupleName + " application.","Application has been successfully");
    
    var bStatus = false;
    
     
    
    bStatus = objApp.login();
    if(bStatus) test.pass("Login to the application","Logged in to the application successfully");
    else test.fail("Login to application","Unable to login to application");
    
    bStatus = objApp.navigateToSalesCustomersScreen();
    if(bStatus) test.pass("Navigate to Sales-->Customer","Navigation successful");
    else test.fail("Navigate to Sales-->Customer","Unable to navigate");
    
    objApp.disableDefaultQueryOnStart(":customers_QMenu",":customers._queryBtn_QToolButton");
    test.pass("Disable 'Query on Start'","Default 'Query on Start' has been disabled");
    
    
   
    
    
    
    
    //Test data of the customer
    /*var dsCustomerDetails = testData.dataset("CustomerDetails.tsv");
    
    objApp.searchAndSelectCustomer(testData.field(dsCustomerDetails[objApp.getCustomerDetailsColNo("Filter Criteria")],"tst_TC09_LoyalityDiscountNull "),testData.field(dsCustomerDetails[objApp.getCustomerDetailsColNo("Value")],"tst_TC09_LoyalityDiscountNull "));
   // objApp.searchAndSelectCustomer(testData.field(dsCustomerDetails[3],"Filter Criteria"),testData.field(dsCustomerDetails[3],"Value"));
    test.pass("Search for the customer " + testData.field(dsCustomerDetails[objApp.getCustomerDetailsColNo("Value")],"tst_TC09_LoyalityDiscountNull "),"Customer details are being fetched successfully");
  
    
    objApp.openNewSalesOrderWindow();
    test.pass("Open new sales order window","New sales order has been successfully launched");
    
    var dsOrderDetails = testData.dataset("OrderDetails.tsv");
    
    objApp.addNewLineItem(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Product Name")],"tst_TC09-LoyalityDiscountNull-1"),testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Product Quantity")],"tst_TC09-LoyalityDiscountNull-1"),testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("Discount Percentage")],"tst_TC09-LoyalityDiscountNull-1"), true, true);
    test.pass("Add new line item","New sales order has been added at the line level successfully");
     objApp.verifyAlltheLineItemValuesOnOrderPage("tst_TC09-LoyalityDiscountNull-1");
    objApp.verifyAlltheLineItemValues("tst_TC09-LoyalityDiscountNull-");
   
    var orderNumber=objApp.saveAndShipOnOrderPage();
    test.pass("Order created successfully with order number = ",orderNumber);
    
    // Navigating to shipping order//
    bStatus = objApp.navigateToIssueShipping(orderNumber);
    if(bStatus) test.pass("Navigate to Ship order","Issue to shipping is done successfully");
    else test.fail("Navigate to Ship Order","Issue to shipping is failed");
    
    var invoiceNumber=objApp.getinvoiceNumber();
    test.pass("invoice number = ",invoiceNumber);
    
    //Navigating to Accounting to post invoice
    bStatus = objApp.PostInvoiceInAccounting(orderNumber);
    if(bStatus) test.pass("Verifying Invoice in Accounting ","Posting invoice is done successfully");
    else test.fail("Verifying Invoice in Accounting","Unable to post the invoice");
    
    //Verifying posted invoice line items through Customer window 
    bStatus = objApp.verifyInvoiceInCustomer(invoiceNumber );
    if(bStatus) test.pass("Verifying Invoice through SalesOrder ","Verify invoice is done sucessfully");
    else test.fail("Verifying Invoice through SalesOrder ","Unable to verify the invoice");
     objApp.VerifyInvoiceLineitems(orderNumber,"tst_TC09-LoyalityDiscountNull-");*/
   
   
}
